import axios from "axios";
import React, { useEffect, useState } from "react";

export const useGetCategoria = (id) => {
  const [pelis, setPelis] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get(
        "https://api.themoviedb.org/3/discover/movie?api_key=3154ff2c55fd7004978f22d03af6834e&language=es&with_genres=" +
          id
      );
        setPelis(response.data.results);
    }
    fetchData();
  }, []);
  return pelis;
};
