import React, { useContext } from "react";
import '@styles/FavoritosItem.scss'
import AppContext from '@context/AppContext';
import { MdDeleteForever } from "react-icons/md";
import FavoritosContext from "@context/favoritosContext";

const FavoritosItem = ({ pelicula,index }) => {
    
  const {eliminarPelicula}=useContext(FavoritosContext)
    const image = `https://image.tmdb.org/t/p/w500${pelicula.poster_path}`

    const handleEliminar=(index)=>{
        eliminarPelicula(index)
    }

    return (
        <div className="favoritos">
            <figure className="favoritosItemFigure">
                <img src={image} alt={image} />
            </figure>
            <h4 className="titleFav">{pelicula.title}</h4>
            <div className="btnC">

            <button className="btn btn-danger" onClick={()=>handleEliminar(pelicula.id)}><MdDeleteForever /></button>
            </div>
        </div>
    );
}

export default FavoritosItem;
