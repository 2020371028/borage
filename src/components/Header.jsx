import React, { useState, useEffect } from 'react';
import '@styles/Header.scss';
import Menu from '@components/Menu';
import menu from '@assets/menu.png'
import logo from '@assets/logo.png'
import MenuMiCuenta from '@components/MenuMiCuenta';
import {AiFillHeart} from 'react-icons/ai'
import MisFavoritos from '@pages/MisFavoritos';

const Header = () => {
  const [toggle, setToggle] = useState(false);
  //Funcion que actualiza el estado
  const handleToggle = () => {
    setToggle(!toggle);
  };

  const [toggle2, setToggle2] = useState(false);
  //Funcion que actualiza el estado
  const handleToggle2 = () => {
    setToggle2(!toggle2);
  };

  useEffect(() => {
    function changeCss() {
      const navElement = document.querySelector('header');

      if (window.scrollY > 50) {
        navElement.style.marginTop = 0;
        navElement.style.position = 'fixed';
        navElement.style.backgroundColor = '#2c2b2b';
        navElement.style.borderRadius = 0;
        navElement.style.width = '100%';
        navElement.style.margin = 'auto';
        navElement.style.padding = 0;
        navElement.style.top = '0';
        navElement.style.boxShadow = '0px 20px 45px rgb(0, 0, 0) inset';
        navElement.style.paddingLeft = "20px";
        navElement.style.paddingRight = "20px";
      } else {
        navElement.style.marginTop = '';
        navElement.style.position = 'absolute';
        navElement.style.backgroundColor = '';
        navElement.style.borderRadius = '';
        navElement.style.width = '';
        navElement.style.boxShadow = '0px 45px 35px rgb(0, 0, 0) inset';
        navElement.style.margin = '';
        navElement.style.padding = '';
        navElement.style.top = '';
      }
    }

    window.addEventListener('scroll', changeCss, false);

    return () => {
      window.removeEventListener('scroll', changeCss, false);
    };
  }, []);

  const [favorito, setFavorito] = useState(false);

  const handleFavorito = () => {
    setFavorito(!favorito);
  }

  return (
    <header className='header' id='header'>
      <title>Navbar</title>

      <img onClick={handleToggle} src={menu} alt='menu' className='menu' />

      <a href='/'>
        <img src={logo} alt='logo1' className='logo1' />
      </a>

      <div className='navbar-izquierda'>
        <ul>
          <li>
            <a href='/'>Inicio</a>
          </li>
          <li>
            <a href='/'>Peliculas</a>
          </li>
          <li>
            <a href='/categorias'>Categorias</a>
          </li>
        </ul>
      </div>
      <div className='navbar-derecha'>
        <ul>
          <li>
            {toggle2 && <MenuMiCuenta />}
          </li>
          <li onClick={handleToggle2} className='navbar-cuenta'>
            <a> Mi Cuenta</a>
          </li>
          <li className='navbar-favoritos'>
            <AiFillHeart size={'40px'} color='var(--bordo3)' onClick={handleFavorito}/>
          </li>
        </ul>
      </div>
      {toggle && <Menu />}
      {favorito && <MisFavoritos/>}
    </header>
  );
};

export default Header;