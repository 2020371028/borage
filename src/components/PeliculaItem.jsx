import React, {useContext, useEffect}from "react";
import '@styles/detallePeli.scss';
import { useNavigate } from "react-router-dom";

const PeliculaItem = ({product}) => {
  
const image = `https://image.tmdb.org/t/p/w500${product.poster_path}`
const navigate = useNavigate();
const handleNavigate = () => {
navigate(`detallepelicula/${product.id}`, {replace: true})
}
  return (
    <div className="pelicula">
      <a onClick={()=>handleNavigate()}>
        <img src={image} alt="" className="imageS"/>
        <p></p>
      </a>
    </div>
      
  );
}

export default PeliculaItem;