import React, { useRef } from "react";
import "@styles/Login.scss";
import logoLo from "@assets/logo.png";
import { useNavigate } from "react-router-dom";

const Login = () => {
  //2- crear constante asignando el valor useRef

  const formulario = useRef(null)

  const handleSubmit = () => {

    const formData = new FormData(formulario.current)

    const data = {
      username: formData.get('email'),
      pass: formData.get('pass')
    }
    console.log(data)
  }

  const navigate = useNavigate()


  return (

    <div className="loginLo">
      <div className="contendor-log">
        <div className="form-containerLo">
          <img src={logoLo} className="logoLo" />
          <form action="" className="form-loginLo" ref={formulario}>
            <label htmlFor="email" className="labelLo">
              Correo Electronico
            </label>
            <input
              type="email"
              className="inputLo"
              placeholder="tristan.m.p@hotmail.com"
              name="email"
            />
            <label htmlFor="email" className="labelLo">
              Contraseña
            </label>
            <input
              type="password"
              className="inputLo"
              placeholder="*********************"
              name="pass"
            />
            <div className="botones">

              <a href="/" className="btn-cr">
                <input type="button" className="boton-principalLo" defaultValue="Iniciar sesión" onClick={handleSubmit} href='/' />
              </a>

              <a href="/categorias" className="btn-cr">
                <button onClick={(() => navigate('/CrearCuenta'))} className="recoveryLo">Crear una cuenta</button>
              </a>
            </div>

          </form>
        </div>
      </div >
    </div >
  );
};

export default Login;
