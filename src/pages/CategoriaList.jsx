import React from "react";
import { useGetCategoria } from "@hooks/useGetCategoria";
import { useParams } from "react-router-dom";
import '@styles/CategoriaList.scss';
import PeliculaItem from "@components/PeliculaItem";
export const CategoriaList = () => {
    const { id } = useParams();
    var title = 'Categoria'

    switch (id) {
        case '27':
            title ='Terror'
            break;
        case '9648':
            title ='Misterio'
            break;
        case '878':
            title ='Ciencia Ficción'
            break;
        case '35':
            title ='Comedia'
            break;
        case '12':
            title ='Aventura'
            break;
        case '28':
            title ='Acción'
            break;
        case '14':
            title ='Fantasía'
            break;
        case '16':
            title='Infantiles'
            break;
        default:
            ''
            break;
    }


    const peliculas = useGetCategoria(id)

    return (

        <div className="content container">
            <br></br>
            <br></br>
            <center>
                <h1 className="titleS">{title}</h1>
            </center>
            <div className="row">
                {peliculas.map((pelicula,index) => {
                    return (
                        <div className="card col-6 col-md-2 col-lg-2 cardsSt">
                            <PeliculaItem product={pelicula} key={index} />
                        </div>
                    )
                })}
            </div>
        </div>

    );
}